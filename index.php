<?php

require 'vendor/autoload.php';
require 'controller/MainController.php';
require 'controller/AccountController.php';
require 'controller/ClientController.php';
require 'controller/ProductController.php';
require 'controller/KeyController.php';
include "vendor/bin/generated-conf/config.php";



$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);

$app = new \Slim\App($c);

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader("Access-Control-Allow-Credentials", "true")
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With')
        ->withHeader('Access-Control-Allow-Headers', 'Content-Type')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

include 'routes.php';

$app->run();