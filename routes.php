<?php
$app->get('/','MainController::home');

$app->post('/login','AccountController::login');

//CLIENT
$app->post('/client','ClientController::newClient');
$app->get('/client','ClientController::getAll');

//PRODUCT
$app->get('/product','ProductController::getAll');

//KEY
$app->get('/key','KeyController::getAll');
$app->get('/last/key','KeyController::getLast');
$app->get('/key/{key}/{product}','KeyController::find');
$app->post('/key','KeyController::newKey');
$app->put('/mac/{key}/{mac}', 'KeyController::updateMac');

