<?php

/**
 * Created by PhpStorm.
 * User: troll173
 * Date: 8/24/2017
 * Time: 10:49 PM
 */
class KeyController{

    public function updateMac($request, $response, $args){
         KeyQuery::create()
            ->filterByPrimaryKey($args['key'])
            ->update(array('Mac'=>$args['mac'], 'Used'=>true));

         $res = array(
             "deleted"=>true
         );
        $response->getBody()->write(json_encode($res));
    }


    public function getAll($request, $response){
        $keys = KeyQuery::create()
            ->orderById("ASC")
            ->limit(10)
            ->joinWithAccount()
            ->joinWithProduct()
            ->find();
        $response->getBody()->write(json_encode($keys->toArray()));

        return $response;
    }

    public function find($request, $response, $args){
        $key = KeyQuery::create()
            ->orderById("ASC")
            ->limit(10)
            ->joinWithAccount()
            ->joinWithProduct()
            ->filterByKey($args['key'])
            ->filterByProductId($args['product'])
            ->find();
        $response->getBody()->write(json_encode($key->toArray()));
        return $response;
    }

    public function getLast($request, $response){
        $key = KeyQuery::create()
            ->orderById("DESC")
            ->findOne();

        $res = array();
        if($key != null){
            $res = $key->toArray();
        }

        $response->getBody()->write(json_encode($res));
        return $response;
    }

    public function newKey($request, $response){
        $product = $request->getParam('Product');
        $akey = $request->getParam('Key');
        $created = $request->getParam('CreatedBy');
        $key = new Key();
        $key->setProductId($product);
        $key->setKey($akey);
        $key->setCreatedBy($created);
        $key->save();
        $response->getBody()->write(json_encode($key->toArray()));
        return $response;
    }

}