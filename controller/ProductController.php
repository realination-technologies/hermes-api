<?php

/**
 * Created by PhpStorm.
 * User: troll173
 * Date: 8/24/2017
 * Time: 7:46 PM
 */
class ProductController
{
    public function getAll($request, $response){
        $products = ProductQuery::create()
            ->orderById("ASC")
            ->limit(10)
            ->find();
        $response->getBody()->write(json_encode($products->toArray()));

        return $response;
    }

}