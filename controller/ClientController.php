<?php

/**
 * Created by PhpStorm.
 * User: troll173
 * Date: 8/24/2017
 * Time: 4:19 PM
 */
class ClientController{

    public function newClient($request, $response) {
        $company = $request->getParam('company');
        $contact_person = $request->getParam('contact_person');
        $contact_number = $request->getParam('contact_number');
        $email = $request->getParam('email');

        $client = new Client();
        $client->setCompany($company);
        $client->setContactPerson($contact_person);
        $client->setContactNumber($contact_number);
        $client->setEmail($email);
        $client->save();

        $response->getBody()->write(json_encode($client->toArray()));

        return $response;
    }


    public function getAll($request, $response){
        $clients = ClientQuery::create()
            ->limit(10)
            ->find();
        $response->getBody()->write(json_encode($clients->toArray()));

        return $response;
    }

}