<?php

/**
 * Created by PhpStorm.
 * User: troll173
 * Date: 8/23/2017
 * Time: 10:10 PM
 */
class AccountController{

    public function login($request, $response){
        $username = $request->getParam('username');
        $password = $request->getParam("password");

      $account = AccountQuery::create()
            ->filterByUsername($username)
            ->filterByPassword($password)
            ->find();

        $response->getBody()->write($account->toJSON());
        return $response;
    }

}